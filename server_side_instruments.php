<?php
function files_only($file, $path)
{
    return $file != '.' && $file != '..' && is_dir($path) ? true : false;
}

function get_server_side_instruments()
{
    $path = "./sounds";

    if (is_dir($path)) {

        if ($handle = opendir($path)) {

            while (($file = readdir($handle)) !== false) {

                if(!is_dir($path . DIRECTORY_SEPARATOR . $file))
                    continue;

                $subpath = $path . '/' . $file;

                if (files_only($file, $path)) {

                    if ($subhandle = opendir($subpath)) {

                        while (($soundfile = readdir($subhandle)) !== false) {

                            if (files_only($soundfile, $subpath) && is_file($subpath . '/' . $soundfile)) {
                                $valid_files[$file][] = $soundfile;
                            }
                        }
                    }
                }
            }
            closedir($handle);
        }

        echo json_encode(array_reverse($valid_files));
    }
}

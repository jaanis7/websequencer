class GUI {
    constructor() {
        /**
         * defines if song is Playing for the bottom left play button
         */
        this.isPlaying = false;

        this.key = 'C';
        this.type = 'major';
        this.firstOctave = 1;
        this.lastOctave = 6;
        this.currentInstrument = 'piano';

    }

    arrayBuilder(y, x) {
        var map = new Array(y);
        for (var i = 0; i < y; i++) {
            map[i] = new Array(x);
            for (var j = 0; j < x; j++) {
                map[i][j] = 0;
            }
        }
        return map;
    }

    /**
     * draws a Grid into the inner main sequencer document
     * @param {*} tones number of tones shown per octave
     * @param {*} octaves number of octaves shown in entire document
     * @param {*} beats number of beats per bar
     * @param {*} bar number of bars in entire document
     */

    /*
        function drawSequencerGrid(tones, octaves, beats, bar) {*/
    drawSequencerGrid() {
        var code = "<table class='notegrid'><tbody>";

        // Left and right captions showing note names - set = 0 to hide;
        var leftCaption = 1;
        var rightCaption = 0;

        var gridSizeX = Settings.bars * 8;

        this.scaleWithOcataves = getScaleWithOctaves(this.key, this.type, this.firstOctave, this.lastOctave);

        //console.log("getScaleWithOctaves('" + this.key +"','"+ this.type +"','"+ this.firstOctave +"','"+ this.lastOctave + "'); ");
        //console.log(this.scaleWithOcataves);
        for (var i = this.scaleWithOcataves.length - 1; i >= 0; i--) {
            code += "<tr ";
            if (i == 0) {
                code += 'class="bb2" style="border-bottom: none"';
            } else {
                if (this.scaleWithOcataves[i].match(this.key)){
                    if(this.key.match('#'))
                        code += 'class="bb2"';
                    else if (!this.scaleWithOcataves[i].match('#'))
                        code += 'class="bb2"';
                }
            }
            code += ">";

            if (leftCaption == 1) {
                code += "<td class='noteCaption' id='caption";
                code += i + "Left'>";
                code += this.scaleWithOcataves[i];
                code += "</td>";
            }

            for (var j = 0; j < gridSizeX; j++) {
                code += "<td id='";
                code += this.scaleWithOcataves[i].replace('#', 'sharp') + "b" + j;

                if (j % 8 == 7) {
                    code += "' class='br4";
                } else if (j % 4 == 3) {
                    code += "' class='br2";
                } else if (j % 2 == 1) {
                    code += "' class='br2";
                }

                code += "'></td>";
            }

            if (rightCaption == 1) {
                code += "<td class='addGrid' id='caption";
                code += i + "Right'>";
                code += this.scaleWithOcataves[i];
                code += "</td>";
            }

            code += "</tr>";
        }
        code += "</tbody></table>";
        document.getElementById("gridframe").innerHTML = code;

        this.updateSequencerGrid();

        // play notes when clicked and add/remove them from the current instruments part
        var allButtons = document.getElementsByTagName('td');
        var melodyButtons = [];

        for (var k = 0; k < allButtons.length; k++) {
            if (allButtons[k].id.charAt(0).localeCompare('r') != 0 &&
                !allButtons[k].className.includes('noteCaption')) {
                melodyButtons.push(allButtons[k]);
            }
        }

        for (var i = 0; i < melodyButtons.length; i++) {
            var button = melodyButtons[i];
            button.addEventListener('click', function () {
                //console.log(this.id);
                var id = this.id.split('b');
                id[0] = id[0].replace('sharp', '#');

                // Play note when clicked
                allParts.playNote(gui.currentInstrument, id[0]);
                if (!allParts.partContains(gui.currentInstrument, [Tone.Time('8n') * id[1], id[0]])) {
                    // Add note to the loop
                    //.addClass('sadf');
                    gui.highlight($(this));
                    allParts.addToPart(gui.currentInstrument, [Tone.Time('8n') * id[1], id[0]]);
                } else {
                    // Remove note from the loop
                    gui.unHighlight(this.id);
                    allParts.removeFromPart(gui.currentInstrument, [Tone.Time('8n') * id[1], id[0]]);
                }
            });
        }

    }


    updateSequencerGrid() {
        var highlitedElements = allParts.parts[allParts.findPartIndex(this.currentInstrument)][1].getEventsIds();
        if (highlitedElements != null) {
            for (var i = 0; i < highlitedElements.length; i++) {
                this.highlight(($('#' + highlitedElements[i])));
            }
        }
    }

    drawDrumkitGrid() {
        var code = "<table class='notegrid'><tbody>";

        var leftCaption = 1;
        var rightCaption = 0;
        //     console.log(Settings.bars);
        var gridSizeX = Settings.bars * 8;

        // Full Drumkit
        // var scaleDrumkit = getDrumkitNotes(true);
        var scaleDrumkit = getDrumkitNotes();

        for (var i = scaleDrumkit.length - 1; i >= 0; i--) {
            code += "<tr ";
            if (i == scaleDrumkit.length - 1) {
                code += ('class="bb2drum" style="border-top: solid black"')
            } else {
                code += ('class="bb2drum"');
            }
            code += ">";

            var currNote = scaleDrumkit[i];
            if (leftCaption == 1) {
                code += "<td class='noteCaption' id='caption";
                code += i + "Left'>";
                if (currNote == 'C1') {
                    code += "<img src='images/bass_drum.png' style='height: 2em; width: auto; margin: 0;'/>";
                } else if (currNote == 'D#1') {
                    code += "<img src='images/snare.png' style='height: 2em; width: auto; margin: 0;'/>";
                } else if (currNote == 'F1') {
                    code += "<img src='images/hihat_closed.png' style='height: 2em; width: auto; margin: 0;'/>";
                } else if (currNote == 'F#1') {
                    code += "<img src='images/hihat_open.png' style='height: 2em; width: auto; margin: 0;'/>";
                }
                code += "</td>";
            }

            // gridSizeX + 1 um bei j = 1 trotzdem auf 16 Takte zu kommen
            for (var j = 0; j < gridSizeX; j++) {
                code += "<td id='r";
                code += scaleDrumkit[i].replace('#', 'sharp') + "b" + j;

                if (j % 8 == 7) {
                    code += "' class='br4";
                } else if (j % 4 == 3) {
                    code += "' class='br2";
                } else if (j % 2 == 1) {
                    code += "' class='br2";
                }

                code += "'></td>";
            }
            /*
                        if (rightCaption == 1) {
                            code += "<td class='noteCaption' id='caption";
                            code += i + "Right'>";
                            if (currNote == 'C1') {
                                code += "<img src='../images/bass_drum.png' style='height: 2em; width: auto; margin: 0;'/>";
                            } else if (currNote == 'D#1') {
                                code += "<img src='../images/snare.png' style='height: 2em; width: auto; margin: 0;'/>";
                            } else if (currNote == 'F1') {
                                code += "<img src='../images/hihat_open.png' style='height: 2em; width: auto; margin: 0;'/>";
                            } else if (currNote == 'F#1') {
                                code += "<img src='../images/hihat_closed.png' style='height: 2em; width: auto; margin: 0;'/>";
                            }
                            code += "</td>";
                        }*/

            code += "</tr>";
        }
        code += "</tbody></table>";
        document.getElementById("drumgridframe").innerHTML = code;

        this.updateDrumkitGrid();


        var allButtons = document.getElementsByTagName('td');
        var rhythmButtons = [];

        for (var k = 0; k < allButtons.length; k++) {
            if (allButtons[k].id.charAt(0).localeCompare('r') == 0 &&
                !allButtons[k].className.includes('noteCaption')) {
                rhythmButtons.push(allButtons[k]);
            }
        }

        for (var i = 0; i < rhythmButtons.length; i++) {
            var button = rhythmButtons[i];
            button.addEventListener('click', function () {
                //console.log(this.id);

                var id = this.id.substring(1).split('b');
                id[0] = id[0].replace('sharp', '#');
                //      console.log(id);
                // Play note when clicked
                allParts.playNote('drumkit', id[0]);
                if (!allParts.partContains('drumkit', [Tone.Time('8n') * id[1], id[0]])) {
                    // Add note to the loop
                    gui.highlight($(this), true);
                    allParts.addToPart('drumkit', [Tone.Time('8n') * id[1], id[0]]);
                } else {
                    // Remove note from the loop
                    gui.unHighlight(this.id);
                    allParts.removeFromPart('drumkit', [Tone.Time('8n') * id[1], id[0]]);
                }
            });
        }


    }

    updateDrumkitGrid() {
        var highlitedElements = allParts.parts[allParts.findPartIndex('drumkit')][1].getEventsIds();
        if (highlitedElements != null) {
            for (var i = 0; i < highlitedElements.length; i++) {
                this.highlight(($('#' + highlitedElements[i])), true);
            }
        }
    }
// TODO id zu jquery objekt ... umbenennen
    highlight(id, rhythm) {
        var instrument;
        if (typeof rhythm === 'undefined' || rhythm == false) {
            instrument = this.currentInstrument;
        } else {
            instrument = 'rhythm';
        }

        if (id instanceof jQuery) {
            if (instrument == 'piano') {
                //console.log(id + "  ###  " + document.getElementById(id));
                id.css("background-color", "#167FFB");
                //document.getElementById(id).style.backgroundColor = ;
            } else if (instrument == 'marimba') {
                id.css("background-color", "#ffc107");
                //document.getElementById(id).style.backgroundColor = "#ffc107";
            } else if (instrument == 'bass') {
                id.css("background-color", "#28a745");
                //document.getElementById(id).style.backgroundColor = "#28a745";
            } else {
                id.css("background-color", "#ff0000");
                //document.getElementById(id).style.backgroundColor = "#ff0000";
            }
        } else {
            console.log("kein jquery object!");

        }
    }

    unHighlight(id) {
        document.getElementById(id).style.backgroundColor = "transparent";
    }

    testFunction() {
        document.getElementById("klappt").innerHTML = "Klappt!";
    }

    /**
     * starts and stops the song
     */
    togglePlaying() {
        if (this.isPlaying == false) {
            document.getElementById("togglePlayBtn").innerHTML = "<span class='glyphicon glyphicon-pause'></span>"
            this.isPlaying = true;
            replay();
        } else {
            document.getElementById("togglePlayBtn").innerHTML = "<span class='glyphicon glyphicon-play'></span> "
            this.isPlaying = false;
            stop();
        }
    }


    /*
    A working alternative for the selection of the instrument.
    Currently not used because it is replaced by a dynamic version that uses jQuery.
     */
    /**
     * changes the active instrument-button from the bottom toolbar
     * (Could later be used to select the current instrument)
     * @param {*} i int number of button/instrument pressed (0/1/2)
     */
    changeInstrument(i) {
        var class0, class1, class2;
        if (i == 0) {
            class0 = "instrument btn btn-primary";
            class1 = "instrument btn btn-light";
            class2 = "instrument btn btn-light";
            this.currentInstrument = 'piano';
            //TODO redraw sequencer Grid
            // allParts.parts[allParts.findPartIndex(this.currentInstrument)][1].events // gibt die in part enthaltenen events zurück
            this.drawSequencerGrid();
        } else if (i == 1) {
            class0 = "instrument btn btn-light";
            class1 = "instrument btn btn-warning";
            class2 = "instrument btn btn-light";
            this.currentInstrument = 'marimba';
            //TODO redraw sequencer Grid

            this.drawSequencerGrid();
        } else if (i == 2) {
            class0 = "instrument btn btn-light";
            class1 = "instrument btn btn-light";
            class2 = "instrument btn btn-success";
            this.currentInstrument = 'bass'
            //TODO redraw sequencer Grid

            this.drawSequencerGrid();
        }
        document.getElementById("instrument0Btn").className = class0;
        document.getElementById("instrument1Btn").className = class1;
        document.getElementById("instrument2Btn").className = class2;

    }

}

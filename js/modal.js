$(function () {
  /**
   * Fires when #toolsButton - button is clicked and the modal dialog opens
   * all settings from gui-object and the settings-class will be transferred to the gui
   * $.on() is necessary because the <option> tags will be created via jQuery
   */
  $('#toolsButton *').on("click", function () {
    // console.log(gui);

    // Setup the <input type="range">
    $('#BPMslider').val(Settings.getBpm());
    $('#BPMvalue').text(Settings.getBpm().toFixed());

    $('select').empty();
    // Fill <select> tags with options
    for (var value = 0; value <= 7; value++) {
      $('#firstOctaveSelect').append("<option value='" + value + "'>" + value + "</option>");

      if (value > 1)
        $('#lastOctaveSelect').append("<option value='" + value + "'>" + value + "</option>");
    }

    var keys = ['C', 'Db(C#)', 'D', 'E', 'F', 'Gb(F#)', 'G', 'Ab(G#)', 'A', 'B'];
    $.each(keys, function (index, value) {
      $('#keySelect').append("<option value='" + value + "'>" + value + "</option>");
    });

    var scales = ['chromatic', 'major', 'minor', 'harmonic minor', 'melodic minor', 'major pentatonic', 'minor pentatonic'];
    $.each(scales, function (index, value) {
      $('#scaleSelect').append("<option value='" + value + "'>" + value + "</option>");
    });

    for (var value = 1; value <= 16; value++) {
      $('#barsSelect').append("<option value='" + value + "'>" + value + "</option>");
    }

    var examples = ['', 'Bossa Nova', 'Reggae', 'Reggae Halftime'];
    $.each(examples, function (index, value) {
      $('#exampleSelect').append("<option value='" + value + "'>" + value + "</option>");
    });

    // Set the correct values from the GUI js CLASS
    $('#keySelect').val(gui.key);
    $('#scaleSelect').val(gui.type);
    $('#firstOctaveSelect').val(gui.firstOctave);
    $('#lastOctaveSelect').val(gui.lastOctave);
    $('#barsSelect').val(Settings.bars);
    $('#exampleSelect').val('');

  });

  /**
   * Fires when the "close button" in the modal is clicked
   * every setting will be transferred to the gui object
   * */
  $('#modalClose *').on("click", function () {
    Settings.setBpm($('#BPMslider').val());

    if ($('#exampleSelect').val() != '') {
      Examples.loadExample($('#exampleSelect').val());
    } else {

      var scaleChanged = false;

      if (gui.key != $('#keySelect').val()) {
        scaleChanged = true;
        gui.key = $('#keySelect').val();
      }
      if (gui.type != $('#scaleSelect').val()) {
        scaleChanged = true;
        gui.type = $('#scaleSelect').val();
      }
      if (gui.firstOctave != parseInt($('#firstOctaveSelect').val())) {
        scaleChanged = true;
        gui.firstOctave = parseInt($('#firstOctaveSelect').val());

      }
      if (gui.lastOctave != parseInt($('#lastOctaveSelect').val())) {
        scaleChanged = true;
        gui.lastOctave = parseInt($('#lastOctaveSelect').val());
      }

      Settings.setBars($('#barsSelect').val());
    }
      gui.drawSequencerGrid();
      gui.drawDrumkitGrid();

    if (scaleChanged) {
      allParts.clearEventsOfMelodyPartsOutsideCurrentScaleWithOctaves();
    }
/*
    if($('#humanizeCheck').is(':checked')){
        // Settings.setHumanization(true); // geht komischer weise nicht
        allParts.setHumanization(true);
      } else {
        // Settings.setHumanization(false); // geht komischer weise nicht
        allParts.setHumanization(false);
      }
*/

  });
});

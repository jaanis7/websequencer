class Settings {
    static applySettings(bpm, bars, humanization) {
        if (typeof bpm === 'undefined') {
            Settings.setBpm(90);
        } else {
            Settings.setBpm(bpm);
        }

        // Tell Transport to loop from beginning of the Timeline
        Tone.Transport.loopStart = 0;
        Tone.Transport.loop = true;


        if (typeof bars === 'undefined') {
            Settings.setBars(1);
        } else {
            Settings.setBars(bars);
        }
        /*
        if (typeof humanization === 'undefined')  {
            Settings.setHumanization(false);
        } else {
            Settings.setHumanization(humanization);
        }
        */
    }

    static setBpm(bpm) {
        Tone.Transport.bpm.value = bpm;
    }
    static getBpm() {
        return Tone.Transport.bpm.value;
    }

    static setBars(bars) {
        if (bars < 1) {
            console.log("setBars(bars) received invalid input, bars must be greater or equal to 1");
        } else {
            Tone.Transport.loopEnd = bars.toString().concat(":0:0");
            this.bars = bars;
        }
    }

    static addBar() {
        Tone.Transport.loopEnd = Tone.Transport.loopEnd + Tone.Time('1m');
    }

    static removeBar() {
        if ((Tone.Transport.loopEnd - Tone.Time('1m')) < Tone.Time('1:0:0')) {
            console.log("Remove bar cannot be executed because it would result in less than 1 bar.");
        } else {
            Tone.Transport.loopEnd = Tone.Transport.loopEnd - Tone.Time('1m');
        }
    }
    /*
    static setHumanization(humanization){
        console.log("got here");
        if(typeof Settings.humanization  === 'undefined' || Settings.humanization != humanization){
            Settings.humanization = humanization;
            allParts.setHumanization(humanization);
        }
    }
    */
}

Settings.applySettings(110, 1);

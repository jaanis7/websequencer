/**
 * Returns an array containing the notes the chromatic scale with key key.
 * @param key String, Key of the scale, f.i. 'C#' or 'D'
 * @return {Array} A String-array containing the Notes of the specified chromatic scale.
 */
function getChromaticScale(key) {
    var cChromatic = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
    key = cChromatic.indexOf(key.toUpperCase());  // get position of key in array
    var keyChromaticScale = [];

    for (let i = 0; i < 12; i++) {
        if ((key + i) < 12)
            keyChromaticScale.push(cChromatic[key + i]);
        else
            keyChromaticScale.push(cChromatic[(key + i) - 12]);
    }
    return keyChromaticScale;
}

/**
 * Returns an Array containing the names of the notes of the scale that is specified by key and type.
 * @param key
 * @param type
 * @return {Array} an Array containing the names of the notes of the scale that is specified by key and type.
 */
function getScale(key, type) {
    var scale = getChromaticScale(key);

    if (type.localeCompare('chromatic') == 0)
        return scale;

    // Aufbau: 1-1-1/2-1-1-1(-1/2), 1 = Ganztonschritt, 1/2 = Halbtonschritt
    if (type.localeCompare('major') == 0) {
        // if key == c, scale contains: c, c#, d, d#, ...
        scale.splice(1, 1); // c, d, d#, ...
        scale.splice(2, 1); // c, d, e, f, f#, ...
        scale.splice(4, 1); // c, d, e, f, g, g#, ...
        scale.splice(5, 1); // c, d, e, f, g, a, a#, b
        scale.splice(6, 1); // c, d, e, f, g, a, b
    }

    // Aufbau: 1-1/2-1-1-1/2-1(-1), 1 = Ganztonschritt, 1/2 = Halbtonschritt
    if (type.localeCompare('minor') == 0 || type.localeCompare('natural minor') == 0) {
        // if key == a, scale contains: a, a#, b, c, ...
        scale.splice(1, 1); // a, b, c, c#, ...
        scale.splice(3, 1); // a, b, c, d, d#, ...
        scale.splice(4, 1); // a, b, c, d, e, f, f#
        scale.splice(6, 1); // a, b, c, d, e, f, g, g#
        scale.splice(7, 1); // a, b, c, d, e, f, g

    }

    // Aufbau: 1-1/2-1-1-1/2-3/2(-1), 1 = Ganztonschritt, 1/2 = Halbtonschritt
    if (type.localeCompare('harmonic minor') == 0) {
        // if key == a, scale contains: a, a#, b, c, ...
        scale.splice(1, 1); // a, b, c, c#, ...
        scale.splice(3, 1); // a, b, c, d, d#, ...
        scale.splice(4, 1); // a, b, c, d, e, f, f#
        scale.splice(6, 1); // a, b, c, d, e, f, g, g#
        scale.splice(6, 1); // a, b, c, d, e, f, g#
    }

    // Aufbau: 1-1/2-1-1-1-1(-1), 1 = Ganztonschritt, 1/2 = Halbtonschritt
    if (type.localeCompare('melodic minor') == 0) {
        // if key == a, scale contains: a, a#, b, c, ...
        scale.splice(1, 1); // a, b, c, c#, ...
        scale.splice(3, 1); // a, b, c, d, d#, ...
        scale.splice(4, 1); // a, b, c, d, e, f, f#
        scale.splice(5, 1); // a, b, c, d, e, f#, g, g#
        scale.splice(6, 1); // a, b, c, d, e, f#, g#
    }

    // Aufbau: 1., 2., 3., 5., 6. Ton der Dur-Tonleiter
    if (type.localeCompare('major pentatonic') == 0) {
        // if key == c, scale contains: c, c#, d, d#, ...
        scale.splice(1, 1); // c, d, d#, ...
        scale.splice(2, 1); // c, d, e, f, f#, ...
        scale.splice(4, 1); // c, d, e, f, g, g#, ...
        scale.splice(5, 1); // c, d, e, f, g, a, a#, b
        scale.splice(6, 1); // c, d, e, f, g, a, b

        scale.splice(3, 1); // c, d, e, g, a, b
        scale.splice(5, 1); // c, d, e, g, a

    }

    // Aufbau: 1., 3., 4., 5., 7. Ton der Moll-Tonleiter
    if (type.localeCompare('minor pentatonic') == 0) {
        // if key == a, scale contains: a, a#, b, c, ...
        scale.splice(1, 1); // a, b, c, c#, ...
        scale.splice(3, 1); // a, b, c, d, d#, ...
        scale.splice(4, 1); // a, b, c, d, e, f, f#
        scale.splice(6, 1); // a, b, c, d, e, f, g, g#
        scale.splice(7, 1); // a, b, c, d, e, f, g

        scale.splice(1, 1); // a, c, d, e, f, g
        scale.splice(4, 1); // a, c, d, e, g
    }

    return scale;
}

/**
 * Returns an octave number to each note of the passed scale beginning with keyOctave for the key of the scale and
 * @param scale an Array containing Strings that represent notes without specifing their octave
 * @param octave number of the octave
 * @return {*} an Array containing Strings that represent the notes of an octave of the passed scale
 *  in scientific notation
 */
function getScaleOctave(scale, keyOctave) {
    var scaleOctave = JSON.parse(JSON.stringify(scale));
    for (let i = 0; i < scale.length; i++) {
        // At C a new octave begins, thus the following notes are in the octave keyOctave + 1
        if (scale[0].localeCompare('C') != 0 && (scaleOctave[i].localeCompare('C') == 0 ||
            (scaleOctave[i].localeCompare('C#') == 0 && scaleOctave[i - 1].localeCompare('C') != 0)))
            keyOctave = keyOctave + 1;
        scaleOctave[i] = scaleOctave[i].concat(keyOctave);
    }
    return scaleOctave;
}

/**
 * Returns an Array containing all notes-names of the passed scale in the range of firstOctave and lastOctave in
 * scientific notation
 * @param scale an Array containing Strings that represent notes without specifing their octave
 * @param firstOctave
 * @param lastOctave
 * @return {Array}  an Array containing all notes-names of the passed scale in the range of firstOctave and lastOctave
 * in scientific notation
 */
function getScaleOctaves(scale, firstOctave, lastOctave) {
    var scaleOctaves = getScaleOctave(scale, firstOctave);
    for (let i = firstOctave + 1; i <= lastOctave; i++) {
        Array.prototype.push.apply(scaleOctaves, getScaleOctave(scale, i));
    }
    return scaleOctaves;
}


/**
 * Returns an Array containing all notes-names of the scale (which is specified by key and type)
 * in the range of firstOctave and lastOctave in scientific notation.
 * @param key key of the scale, e.g. 'C', or 'F#'
 * @param type String, type of the scale, one of the following values: 'chromatic', 'major', minor', 'harmonic minor',
 *          'major pentatonic', 'minor pentatonic'
 * @param firstOctave
 * @param lastOctave
 * @return {Array}  an Array containing all notes-names of the specified scale in the range of
 * firstOctave and lastOctave in scientific notation
 */
function getScaleWithOctaves(key, type, firstOctave, lastOctave) {
    return getScaleOctaves(getScale(key, type), firstOctave, lastOctave);
}

/**
 * Returns a scale where each note represents an instrument of the drumkit. If all is undefined or false the the scale
 * is reduced to the 4 most important instruments.
 * @param all boolean request all available instruments.
 * @return {string[]} a scale where each note represents an instrument of the drumkit. If all is undefined or
 * false the the scale is reduced to the 4 most important instruments.
 */
function getDrumkitNotes(all) {
    if (typeof all === 'undefined') {
        all = false;
    }
    if (all) {
        return ['C1', 'D1', 'D#1', 'E1', 'F1', 'F#1', 'A1', 'G1', 'B1'];
    } else {
        return ['C1', 'D#1', 'F1', 'F#1'];
    }
}

/**
 * Returns a scale where each note represents an instrument of the drumkit. Each note is in a subarray that also
 * contains the name of the represented instrument. If all is undefined or false the the scale
 * is reduced to the 4 most important instruments.
 * @param all boolean request all available instruments.
 * @return {string[][]} a scale where each note represents an instrument of the drumkit. Each note is in a subarray that also
 * contains the name of the represented instrument. If all is undefined or false the the scale
 * is reduced to the 4 most important instruments.
 */
function getDrumkitNotesWithNames(all) {
    if (typeof all === 'undefined') {
        all = false;
    }
    if (all) {
        return [['C1', 'Kick'], ['D1', 'Snare'], ['D#1', 'Snare (Rim Shot)'], ['E1', 'Tom'], ['F1', 'HiHat closed'],
            ['F#1', 'HiHat open'], ['A1', 'Cymbol'], ['G1', 'Crash'], ['B1', 'Shaker']];
    } else {
        return [['C1', 'Kick'], ['D#1', 'Snare (Rim Shot)'], ['F1', 'HiHat closed'], ['F#1', 'HiHat open']];
    }
}

/**
 * Returns a scale where each note represents an instrument of the percussion. If all is undefined or false the scale
 * is reduced to  the 4 most important instruments.
 * @param all boolean request all available instruments.
 * @return {string[]} a scale where each note represents an instrument of the percussion. If all is undefined or
 * false the the scale is reduced to the 4 most important instruments.
 */
function getPercussionNotes(all) {
    if (typeof all === 'undefined') {
        all = false;
    }
    if (all) {
        return ['C3', 'D3', 'D#3', 'E3', 'G3', 'G#3', 'B3'];
    } else {
        return ['C3', 'D3', 'E3', 'B3'];
    }
}

/**
 * Returns a scale where each note represents an instrument of the percussion. Each note is in a subarray that also
 * contains the name of the represented instrument. If all is undefined or false the the scale
 * is reduced to the 4 most important instruments.
 * @param all boolean request all available instruments.
 * @return {string[][]} a scale where each note represents an instrument of the percussion. Each note is in a subarray
 * that also contains the name of the represented instrument. If all is undefined or false the the scale
 * is reduced to the 4 most important instruments.
 */
function getPercussionNotesWithNames(all) {
    if (typeof all === 'undefined') {
        all = false;
    }
    if (all) {
        return [['C3', 'Conga deep'], ['D3', 'Bongo high'], ['D#3', 'Bongo high muted'], ['E3', 'Bongo'],
            ['G3', 'Bongo slap'], ['G#3', 'Djembe'], ['B3', 'Oyster shaker']];
    } else {
        return [['C3', 'Conga deep'], ['D3', 'Bongo high'], ['E3', 'Bongo'], ['B3', 'Oyster shaker']];
    }
}
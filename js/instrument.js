class Instrument {

    constructor(instrument) {
        this.instrument = instrument;

        this.loc = window.location;

        this.filename = this.loc.pathname.substring(this.loc.pathname.lastIndexOf('/') + 1);

        var temp = this.loc.href.replace(this.filename, "");

        this.path = temp.replace("#", "");



        this.loadInstrument(instrument)
    }

    loadInstrument(instrument) {
        var scale = {};

        if (ssi.hasOwnProperty(instrument)) {
            for (var i = 0; i < ssi[instrument].length; i++) {
                // console.log(ssi[instrument].length);
                if (ssi[instrument][i]) {
                    var item = ssi[instrument][i];
                    var sliced = item.slice(0, -4).replace('sharp', '#');
                    scale[sliced] = this.path + "sounds/" + instrument + "/" + item;
                }
            }
        }


        /** Erstellen des Samplers */
        var sampler = new Tone.Sampler(scale).toMaster();

        this.sampler = sampler;
    }
}

class Part {
  constructor(instrument, noteLength, events, volume, humanization, startAt /*,loopEnd, loop*/) {

    if (typeof events === 'undefined') {
      events = [];
    }

    // noteLength defines how long each note is. Alternative way would be to give the user the ability to change
    // the length of each note. Then the array events would contain arrays with time, note, length.
    if (typeof noteLength === 'undefined') {
      noteLength = '8n';
    }

    this.instrument = instrument; // safe instrument for id creation
    this.loadSampler(instrument);

    this.createTonePart(this.samplerOfInstrument, noteLength, events);
/*
    if (!(typeof humanization === 'undefined')) {
      this.setHumanization(humanization);
    }*/

    if (typeof volume === 'undefined') {
      this.volume = 0;
    }
    // this.updateVolume();

    this.addToTransportTimeline(startAt);

  }

  loadSampler(instrument) {
    var inst = new Instrument(instrument);
    this.samplerOfInstrument = inst.sampler;
    /*
    // load sampler
    console.log(instrument);
    if (instrument.localeCompare("percussion") != 0) {
        this.samplerOfInstrument = new Tone.Sampler({
            //'C2': 'C2.mp3',
            'C3': 'C3.mp3',
            'C4': 'C4.mp3',
            'C5': 'C5.mp3'
            //'C6': 'C6.mp3',
        }, {
            'baseUrl': "/sounds".concat("/", instrument, "/"), "fadeout": '32n', "volume": this.volume, envelope: {
                attack: 0.001,
                decay: '64n',
                sustain: '64n',
                release: '64n'
            }
        }).toMaster();
    }
    else {
        this.samplerOfInstrument = new Tone.Sampler({
            'D1': 'Kick03.mp3',
            'D4': 'Hiphop101 -Snare - (3).mp3',
            'D5': 'HiHat11.mp3',
            'B6': 'Air Bongo.mp3'
        }, {
            'baseUrl': "/sounds/percussion/", "fadeout": '64n', "volume": this.volume, envelope: {
                attack: 0.001,
                decay: '64n',
                sustain: '32n',
                release: '64n'
            }
        }).toMaster();
    }
    */
  }

  createTonePart(samplerOfInstrument, noteLength, events) {
    this.part = new Tone.Part(function (time, note) {
      samplerOfInstrument.triggerAttackRelease(note, noteLength, time)
    }, events);
    this.events = events;
  }

  updateEvents(events) {
    this.removeAllEvents();
    this.addEvents(events);
  }

  addEvents(events) {
    for (let i = 0, len = events.length; i < len; i++) {
      this.addEvent(events[i]);
    }
  }

  contains(event) {
    return this.events.some(function (e) {
      if (typeof event[0] == 'number')
        event[0] = Tone.Time(event[0]).toBarsBeatsSixteenths();
      if (typeof e[0] == 'number')
        e[0] = Tone.Time(e[0]).toBarsBeatsSixteenths();
      return e[0].localeCompare(event[0]) == 0 && e[1].localeCompare(event[1]) == 0;
    });
  }


  addEvent(event) {
    this.part.add(event[0], event[1]);
    if (!this.contains(event)) {
      this.events.push(event);
    }
  }

  removeEvent(event) {
    this.part.remove(event[0], event[1]);

    if (this.contains(event)) {
      // Find index of the event in this.events
      let index = this.events.findIndex(function (e) {
        return e[0].localeCompare(event[0]) == 0 && e[1].localeCompare(event[1]) == 0;
      });
      // Remove event from this.events
      if (index > -1) {
        this.events.splice(index, 1);
      }
    }
  }

  removeAllEvents() {
    this.part.removeAll();
    this.events = [];
  }

  setVolume(dB) {
    this.part.setVolume(dB);
  }

  setHumanization(humanization) {
    // console.log(this.instrument.concat("setHumanize: ", humanization));
    this.part.humanize = humanization;
  }


  /**
   * Add part to transport to the Transport's timeline at the TimelinePosition startAt
   */
  addToTransportTimeline(startAt) {
    if (typeof startAt === 'undefined') {
      // start at beginning of the Transport's Timeline
      startAt = 0;
    }
    this.part.start(startAt);
  }

  // GUI related

  getEventsIds() {
    var prefix = '';
    if(this.instrument.localeCompare('drumkit') == 0 ||
        this.instrument.localeCompare('percussion') == 0)
      prefix = 'r';

    var events = [];
    this.events.forEach(function (event) {
      var time = Tone.Time(event[0]).toBarsBeatsSixteenths().split(':');
      var beat = time[0] * 8 + time[1] * 2 + time[2] * 0.5;
      var pitch = event[1].replace('#', 'sharp');
      events.push(prefix.concat(pitch, "b", beat));
    });
    return events;
  }

  clearEventsOutsideCurrentScaleWithOctaves() {
    var self = this;
    this.events.some(function (e) {
      if (typeof e[0] == 'number')
        e[0] = Tone.Time(e[0]).toBarsBeatsSixteenths();
      if (!gui.scaleWithOcataves.includes(e[1])) {
        self.removeEvent(e);
      }
    });
  }


}

class PartsGroup {
  constructor() {
    /*        console.log(ssi);
            $.each(ssi, function (key, value) {
                console.log(key);
                this.parts.push([key, new Part(key)]);
            });
            console.log(this.parts);
    */
    this.parts = [
      ['piano', new Part('piano')],
      ['marimba', new Part('marimba')],
      ['bass', new Part('bass')],
      // ['percussion', new Part('percussion')],
      ['drumkit', new Part('drumkit')]
    ];
  }

  addPart(instrument, events, partName) {
    if (typeof partName === 'undefined') {
      partName = instrument;
    }
    this.parts.push([partName, new Part(instrument, events)]);
  }

  removePart(partName) {
    let partIndex = this.findPartIndex(partName);
    // Delete part, a.o. remove it from Transport.Timeline
    this.parts[partIndex][1].part.dispose();
    // Remove gap in the array this.parts
    this.parts[partIndex][1] = null;
    this.parts.slice(partIndex, partIndex + 1);
    // When allowing to have multiple parts for an instrument an additional parameter instrument will be needed.
  }

  findPartIndex(partName) {
    return this.parts.findIndex(function (element) {
      return element[0].localeCompare(partName) == 0;
    });
  }

  partContains(partName, event) {
    let partIndex = this.findPartIndex(partName);
    return this.parts[partIndex][1].contains(event);
  }

  addToPart(partName, event) {
    let partIndex = this.findPartIndex(partName);
    this.parts[partIndex][1].addEvent(event);
  }

  removeFromPart(partName, event) {
    let partIndex = this.findPartIndex(partName);
    this.parts[partIndex][1].removeEvent(event);
  }


  updatePart(partName, events) {
    let partIndex = this.findPartIndex(partName);
    this.parts[partIndex][1].updateEvents(events);
  }

  clearPart(partName) {
    let partIndex = this.findPartIndex(partName);
    this.parts[partIndex][1].removeAllEvents();
  }

  clearAllParts() {
    for (let i = 0, len = this.parts.length; i < len; i++) {
      this.parts[i][1].removeAllEvents();
    }
  }

  clearEventsOfMelodyPartsOutsideCurrentScaleWithOctaves(){
    for (let i = 0, len = this.parts.length; i < len; i++) {
      if (this.parts[i][0].localeCompare('drumkit') != 0 && this.parts[i][0].localeCompare('percussion') != 0)
        this.parts[i][1].clearEventsOutsideCurrentScaleWithOctaves();
    }
  }

  /**
   * Immediatly plays the the specified note using the sampler of the instrument
   * @param instrument
   * @param pitch
   * @param noteLength
   */
  playNote(instrument, pitch, noteLength) {
    if (typeof noteLength === 'undefined') {
      noteLength = '8n';
    }

    let partIndex = this.findPartIndex(instrument);
    this.parts[partIndex][1].samplerOfInstrument.triggerAttackRelease(pitch, noteLength);
  }

  setHumanization(humanization){
    for (let i = 0, len = this.parts.length; i < len; i++) {
      this.parts[i][1].setHumanization(humanization)
    }
  }
}


/**
 * Plays the loop from the beginning
 */
function replay() {
  Tone.Transport.stop();
  Tone.Transport.start();
}

/**
 * Plays the loop continuing at the current TimelinePosition.
 */
function play() {
  Tone.Transport.start();
}

function stop() {
  Tone.Transport.stop();
}

function pause() {
  Tone.Transport.pause()
}


/**
 * Provides examples
 */
class Examples {
  static loadExample(example, notesOnly){
    if(example == 'Bossa Nova')
        Examples.loadBossaNovaExample(notesOnly);
    else if(example == 'Reggae')
      Examples.loadReggaeExample(notesOnly);
    else if(example == 'Reggae Halftime')
      Examples.loadReggaeExampleHalftime(notesOnly);
  }

  static loadBossaNovaExample(notesOnly) {

    Examples.bossaNovaExample = [
      ["pianoEvents", [
        // Bar 1
        ["0:0:0", "A3"], ["0:0:0", "C4"], ["0:0:0", "E4"], ["0:0:0", "G4"],
        ["0:1:0", "A3"], ["0:1:0", "C4"], ["0:1:0", "E4"], ["0:1:0", "G4"],
        ["0:2:2", "A3"], ["0:2:2", "C4"], ["0:2:2", "E4"], ["0:2:2", "G4"],
        ["0:3:2", "A3"], ["0:3:2", "C4"], ["0:3:2", "E4"], ["0:3:2", "G4"],
        // Bar 2
        ["1:0:2", "A3"], ["1:0:2", "C4"], ["1:0:2", "E4"], ["1:0:2", "G4"],
        ["1:2:0", "A3"], ["1:2:0", "C4"], ["1:2:0", "E4"], ["1:2:0", "G4"],
        ["1:3:0", "A3"], ["1:3:0", "C4"], ["1:3:0", "E4"], ["1:3:0", "G4"],
        // Bar 3
        ["2:0:0", "B3"], ["2:0:0", "D4"], ["2:0:0", "F4"], ["2:0:0", "A4"],
        ["2:1:0", "B3"], ["2:1:0", "D4"], ["2:1:0", "F4"], ["2:1:0", "A4"],
        ["2:2:2", "B3"], ["2:2:2", "D4"], ["2:2:2", "F4"], ["2:2:2", "A4"],
        ["2:3:2", "B3"], ["2:3:2", "D4"], ["2:3:2", "F4"], ["2:3:2", "A4"],
        // Bar 4
        ["3:0:2", "B3"], ["3:0:2", "D4"], ["3:0:2", "F4"], ["3:0:2", "A4"],
        ["3:2:0", "B3"], ["3:2:0", "D4"], ["3:2:0", "F4"], ["3:2:0", "A4"],
        ["3:3:0", "B3"], ["3:3:0", "D4"], ["3:3:0", "F4"], ["3:3:0", "A4"],

        // Bar 5
        ["4:0:0", "A#3"], ["4:0:0", "D4"], ["4:0:0", "F4"], ["4:0:0", "A4"],
        ["4:1:0", "A#3"], ["4:1:0", "D4"], ["4:1:0", "F4"], ["4:1:0", "A4"],
        ["4:2:2", "A#3"], ["4:2:2", "D4"], ["4:2:2", "F4"], ["4:2:2", "A4"],
        ["4:3:2", "A#3"], ["4:3:2", "D4"], ["4:3:2", "F4"], ["4:3:2", "A4"],
        // Bar 6
        ["5:0:2", "A#3"], ["5:0:2", "C#4"], ["5:0:2", "E4"], ["5:0:2", "G#4"],
        ["5:2:0", "A#3"], ["5:2:0", "C#4"], ["5:2:0", "E4"], ["5:2:0", "G#4"],
        ["5:3:0", "A#3"], ["5:3:0", "C#4"], ["5:3:0", "E4"], ["5:3:0", "G#4"],
        // Bar 7
        ["6:0:0", "A3"], ["6:0:0", "C4"], ["6:0:0", "E4"], ["6:0:0", "G4"],
        ["6:1:0", "A3"], ["6:1:0", "C4"], ["6:1:0", "E4"], ["6:1:0", "G4"],
        ["6:2:2", "A3"], ["6:2:2", "C4"], ["6:2:2", "E4"], ["6:2:2", "G4"],
        ["6:3:2", "A3"], ["6:3:2", "C4"], ["6:3:2", "E4"], ["6:3:2", "G4"],
        // Bar 8
        ["7:0:2", "A#3"], ["7:0:2", "C#4"], ["7:0:2", "E4"], ["7:0:2", "G#4"],
        ["7:2:0", "A#3"], ["7:2:0", "C#4"], ["7:2:0", "E4"], ["7:2:0", "G#4"],
        ["7:3:0", "A#3"], ["7:3:0", "C#4"], ["7:3:0", "E4"], ["7:3:0", "G#4"]

      ]],
      ["marimbaEvents", [

        // Bar 1
        ["0:0:0", "G5"],
        ["0:1:2", "E5"],
        ["0:2:0", "E5"],
        ["0:2:2", "D5"],
        ["0:3:2", "G5"],
        // Bar 2
        ["1:1:0", "E5"],
        ["1:1:2", "E5"],
        ["1:2:2", "E5"],
        ["1:3:0", "D5"],
        ["1:3:2", "G5"],
        // Bar 3
        ["2:1:0", "E5"],
        ["2:2:0", "E5"],
        ["2:3:0", "D5"],
        ["2:3:2", "G5"],
        // Bar 4
        ["3:0:2", "G5"],
        ["3:1:0", "E5"],
        ["3:1:2", "E5"],
        ["3:2:2", "E5"],
        ["3:3:0", "D5"],
        ["3:3:2", "F5"],

        // Bar 5
        ["4:0:2", "D5"],
        ["4:1:2", "D5"],
        ["4:2:2", "D5"],
        ["4:3:0", "C5"],
        ["4:3:2", "E5"],
        // Bar 6
        ["5:0:2", "C5"],
        ["5:1:2", "C5"],
        ["5:2:2", "C5"],
        ["5:3:0", "A#4"],
        // Bar 7
        ["6:1:0", "C5"]


      ]],
      ["bassEvents", [
        // Bar 1
        ["0:0:0", "F1"],
        ["0:1:2", "C2"],
        ["0:2:0", "C2"],
        ["0:3:2", "F1"],

        // Bar 2
        ["1:0:0", "F1"],
        ["1:1:2", "C2"],
        ["1:2:0", "C2"],
        ["1:3:2", "G1"],

        // Bar 3
        ["2:0:0", "G1"],
        ["2:1:2", "D2"],
        ["2:2:0", "D2"],
        ["2:3:2", "G1"],

        // Bar 4
        ["3:0:0", "G1"],
        ["3:1:2", "D2"],
        ["3:2:0", "D2"],
        ["3:3:2", "G1"],

        // Bar 5
        ["4:0:0", "G1"],
        ["4:1:2", "D2"],
        ["4:2:0", "D2"],
        ["4:3:2", "F#1"],

        // Bar 6
        ["5:0:0", "F#1"],
        ["5:1:2", "D2"],
        ["5:2:0", "D2"],
        ["5:3:2", "F1"],

        // Bar 6
        ["6:0:0", "F1"],
        ["6:1:2", "C2"],
        ["6:2:0", "C2"],
        ["6:3:2", "F#1"],

        // Bar 7
        ["7:0:0", "F#1"],
        ["7:1:2", "D2"],
        ["7:2:0", "D2"],
        ["7:3:2", "F1"],

      ]],
      ["drumKitEvents", [
        // Bar 1
        // Kick
        ["0:0:0", "C1"],

        ["0:1:2", "C1"],
        ["0:2:0", "C1"],

        ["0:3:2", "C1"],


        // Snare
        ["0:0:0", "D#1"],
        ["0:1:2", "D#1"],
        ["0:3:0", "D#1"],


        // HiHat
        ["0:0:0", "F1"], ["0:0:2", "F1"],
        ["0:1:0", "F1"], ["0:1:2", "F1"],
        ["0:2:0", "F1"], ["0:2:2", "F1"],
        ["0:3:0", "F1"], ["0:3:2", "F1"],

        // Bar 2
        // Kick
        ["1:0:0", "C1"],

        ["1:1:2", "C1"],
        ["1:2:0", "C1"],

        ["1:3:2", "C1"],

        // Snare
        ["1:1:0", "D#1"],
        ["1:2:2", "D#1"],

        // HiHat closed
        ["1:0:0", "F1"], ["1:0:2", "F1"],
        ["1:1:0", "F1"], ["1:1:2", "F1"],
        ["1:2:0", "F1"], ["1:2:2", "F1"],
        ["1:3:0", "F1"], ["1:3:2", "F1"],

        // Bar 3
        // Kick
        ["2:0:0", "C1"],

        ["2:1:2", "C1"],
        ["2:2:0", "C1"],

        ["2:3:2", "C1"],


        // Snare
        ["2:0:0", "D#1"],
        ["2:1:2", "D#1"],
        ["2:3:0", "D#1"],


        // HiHat
        ["2:0:0", "F1"], ["2:0:2", "F1"],
        ["2:1:0", "F1"], ["2:1:2", "F1"],
        ["2:2:0", "F1"], ["2:2:2", "F1"],
        ["2:3:0", "F1"], ["2:3:2", "F1"],

        // Bar 4
        // Kick
        ["3:0:0", "C1"],

        ["3:1:2", "C1"],
        ["3:2:0", "C1"],

        ["3:3:2", "C1"],

        // Snare
        ["3:1:0", "D#1"],
        ["3:2:2", "D#1"],

        // HiHat closed
        ["3:0:0", "F1"], ["3:0:2", "F1"],
        ["3:1:0", "F1"], ["3:1:2", "F1"],
        ["3:2:0", "F1"], ["3:2:2", "F1"],
        ["3:3:0", "F1"], ["3:3:2", "F1"],

        // Bar 5
        // Kick
        ["4:0:0", "C1"],

        ["4:1:2", "C1"],
        ["4:2:0", "C1"],

        ["4:3:2", "C1"],


        // Snare
        ["4:0:0", "D#1"],
        ["4:1:2", "D#1"],
        ["4:3:0", "D#1"],


        // HiHat
        ["4:0:0", "F1"], ["4:0:2", "F1"],
        ["4:1:0", "F1"], ["4:1:2", "F1"],
        ["4:2:0", "F1"], ["4:2:2", "F1"],
        ["4:3:0", "F1"], ["4:3:2", "F1"],

        // Bar 6
        // Kick
        ["5:0:0", "C1"],

        ["5:1:2", "C1"],
        ["5:2:0", "C1"],

        ["5:3:2", "C1"],

        // Snare
        ["5:1:0", "D#1"],
        ["5:2:2", "D#1"],

        // HiHat closed
        ["5:0:0", "F1"], ["5:0:2", "F1"],
        ["5:1:0", "F1"], ["5:1:2", "F1"],
        ["5:2:0", "F1"], ["5:2:2", "F1"],
        ["5:3:0", "F1"], ["5:3:2", "F1"],

        // Bar 7
        // Kick
        ["6:0:0", "C1"],

        ["6:1:2", "C1"],
        ["6:2:0", "C1"],

        ["6:3:2", "C1"],


        // Snare
        ["6:0:0", "D#1"],
        ["6:1:2", "D#1"],
        ["6:3:0", "D#1"],


        // HiHat
        ["6:0:0", "F1"], ["6:0:2", "F1"],
        ["6:1:0", "F1"], ["6:1:2", "F1"],
        ["6:2:0", "F1"], ["6:2:2", "F1"],
        ["6:3:0", "F1"], ["6:3:2", "F1"],

        // Bar 8
        // Kick
        ["7:0:0", "C1"],

        ["7:1:2", "C1"],
        ["7:2:0", "C1"],

        ["7:3:2", "C1"],

        // Snare
        ["7:1:0", "D#1"],
        ["7:2:2", "D#1"],

        // HiHat closed
        ["7:0:0", "F1"], ["7:0:2", "F1"],
        ["7:1:0", "F1"], ["7:1:2", "F1"],
        ["7:2:0", "F1"], ["7:2:2", "F1"],
        ["7:3:0", "F1"], ["7:3:2", "F1"],


      ]],
      ["settings", 110, 8]
    ];

    allParts.updatePart('piano', Examples.bossaNovaExample[0][1]);
    allParts.updatePart('marimba', Examples.bossaNovaExample[1][1]);
    allParts.updatePart('bass', Examples.bossaNovaExample[2][1]);
    allParts.updatePart('drumkit', Examples.bossaNovaExample[3][1]);

    if (typeof notesOnly === 'undefined' || notesOnly == false) {
      Settings.applySettings(Examples.bossaNovaExample[4][1], Examples.bossaNovaExample[4][2]);
    }

    gui.firstOctave = 1;
    gui.lastOctave = 6;
    gui.type = "chromatic";
    gui.key = "C";
  }

  static loadReggaeExample(notesOnly) {
    Examples.reggaeExample = [
      ["pianoEvents", [
        ["0:0:2", "A4"], ["0:0:2", "C5"], ["0:0:2", "E5"],
        ["0:1:2", "C4"], ["0:1:2", "E4"], ["0:1:2", "G4"],
        ["0:2:2", "D4"], ["0:2:2", "F4"], ["0:2:2", "A4"],
        ["0:3:2", "D4"], ["0:3:2", "F4"], ["0:3:2", "A4"],
        ["1:0:2", "A4"], ["1:0:2", "C5"], ["1:0:2", "E5"],
        ["1:1:2", "C4"], ["1:1:2", "E4"], ["1:1:2", "G4"],
        ["1:2:2", "D4"], ["1:2:2", "F4"], ["1:2:2", "A4"],
        ["1:3:2", "D4"], ["1:3:2", "F4"], ["1:3:2", "A4"]
      ]],
      ["marimbaEvents", []],
      ["bassEvents", [
        ["0:0:0", "A1"],
        ["0:2:0", "D1"],
        ["0:3:0", "D2"],

        ["1:0:0", "A1"],
        ["1:2:0", "A1"],
        ["1:3:0", "C2"], ["1:3:2", "D2"]
      ]],
      ["drumKitEvents", [
        // Kick
        ["0:0:0", "C1"],
        ["0:0:2", "C1"],

        // Snare
        ["0:1:0", "D#1"],
        ["0:3:0", "D#1"],

        // HiHat
        ["0:0:2", "F1"],
        ["0:1:2", "F1"],
        ["0:2:0", "F1"], ["0:2:2", "F1"],
        ["0:3:2", "F1"],

        // Bar 2
        // Kick
        ["1:0:0", "C1"],
        ["1:0:2", "C1"],

        // Snare
        ["1:1:0", "D#1"],
        ["1:3:0", "D#1"],

        // HiHat closed
        ["1:0:2", "F1"],
        ["1:1:2", "F1"],
        ["1:2:0", "F1"], ["1:2:2", "F1"],

        ["1:3:2", "F#1"],
      ]],
      ["settings", 100, 2]
    ];

    allParts.updatePart('piano', Examples.reggaeExample[0][1]);
    allParts.updatePart('marimba', Examples.reggaeExample[1][1]);
    allParts.updatePart('bass', Examples.reggaeExample[2][1]);
    allParts.updatePart('drumkit', Examples.reggaeExample[3][1]);

    if (typeof notesOnly === 'undefined' || notesOnly == false) {
      Settings.applySettings(Examples.reggaeExample[4][1], Examples.reggaeExample[4][2]);
    }


    gui.firstOctave = 1;
    gui.lastOctave = 6;
    gui.type = "major";
    gui.key = "C";

  }

  static loadReggaeExampleHalftime(notesOnly) {
    Examples.reggaeExampleHalftime = [
      ["pianoEvents", [
        ["0:1:0", "E4"], ["0:1:0", "G4"], ["0:1:0", "B4"],
        ["0:3:0", "E4"], ["0:3:0", "G4"], ["0:3:0", "B4"],
        ["1:1:0", "D4"], ["1:1:0", "F4"], ["1:1:0", "A4"],
        ["1:3:0", "D4"], ["1:3:0", "F4"], ["1:3:0", "A4"]
      ]],
      ["marimbaEvents", [
        /*["0:0:2", "E3"],
        ["0:1:2", "E3"],
        ["0:2:2", "E3"],
        ["0:3:2", "E3"],

        ["1:0:2", "D3"],
        ["1:1:2", "D3"],
        ["1:2:2", "D3"],
        ["1:3:2", "D3"]*/
      ]],
      ["bassEvents", [
        ["0:0:0", "E1"],
        ["0:2:0", "B1"],
        ["0:3:2", "C2"],

        ["1:0:0", "D1"],
        ["1:2:0", "A1"],
        ["1:2:2", "A1"],
        ["1:3:0", "A1"],
        ["1:3:2", "D1"]

      ]],
      ["drumKitEvents", [
        // Kick
        ["0:0:0", "C1"],

        // Snare
        ["0:2:0", "D#1"],

        // HiHat
        ["0:0:0", "F1"], ["0:0:2", "F1"],
        ["0:1:0", "F1"], ["0:1:2", "F1"],
        ["0:2:0", "F1"], ["0:2:2", "F1"],
        ["0:3:0", "F1"], ["0:3:2", "F1"],

        // HiHat open


        // Bar 2
        // Kick
        ["1:0:0", "C1"],
        ["1:1:0", "C1"],
        ["1:3:2", "C1"],

        // Snare
        ["1:2:0", "D#1"],


        // HiHat closed
        ["1:0:0", "F1"], ["1:0:2", "F1"],
        ["1:1:0", "F1"], ["1:1:2", "F1"],
        ["1:2:0", "F1"], ["1:2:2", "F1"],
        ["1:3:0", "F1"],

        // HiHat open
        ["1:3:2", "F#1"]
      ]],
      ["settings", 140, 2]
    ];


    allParts.updatePart('piano', Examples.reggaeExampleHalftime[0][1]);
    allParts.updatePart('marimba', Examples.reggaeExampleHalftime[1][1]);
    allParts.updatePart('bass', Examples.reggaeExampleHalftime[2][1]);
    allParts.updatePart('drumkit', Examples.reggaeExampleHalftime[3][1]);

    if (typeof notesOnly === 'undefined' || notesOnly == false) {
      Settings.applySettings(Examples.reggaeExampleHalftime[4][1], Examples.reggaeExampleHalftime[4][2]);
    }

    gui.firstOctave = 1;
    gui.lastOctave = 6;
    gui.type = "major";
    gui.key = "C";
  }
}


allParts = new PartsGroup();

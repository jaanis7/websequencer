<!doctype html>
<html lang="de">
<!-- Base document based on starter template from https://getbootstrap.com/docs/4.1/getting-started/introduction/ -->

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">-->
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>
    <!-- Bootstrap CSS -->

    <!-- TODO: Ausschließlich Bootsrap 4 einbinden und Klassen der alten Versionen ersetzen -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
    <title>Websequencer</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

<div id="content">

    <!-- inner window -->
    <div id="gridframe">
        <!--overwritten by javascript-->
    </div>
    <div id="drumgridframe">
        <!--overwritten by javascript-->
    </div>

    <div class="invisible">

        <div class="btn-block" role="toolbar">

            <button type="button" class="btn btn-outline-primary float-left">
                <span class="glyphicon glyphicon-play"></span>
            </button>

            <div class="btn-group mr-2" role="group">
                <button type="button" class="btn btn-primary">Piano</button>
                <button type="button" class="btn btn-light">Marimba</button>
                <button type="button" class="btn btn-light">Bass</button>
            </div>

            <!--TO do-->
            <button type="button" class="btn btn-light">
                <span class="glyphicon glyphicon-trash"></span>
            </button>

            <button id="toolsButton" type="button" class="btn btn-outline-primary float-right">
                <span class="glyphicon glyphicon-cog"></span>
            </button>

        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title">Settings</h1>
                </div>
                <div class="modal-body" id="settings">

                    <!-- Content-->
                    <div class="settingsMargin">
                        <div class="settingsWindow">
                            <form>
                                <div class="row">
                                    <div class="col">
                                        <h2 class="optionsLabel float-left">BPM</h2>
                                    </div>
                                    <div class="col">
                                        <h2>
                                            <span id="BPMvalue"></span>
                                        </h2>
                                    </div>
                                </div>
                                <div class="slidecontainer">
                                    <input id="BPMslider" class="slider" type="range" min="40" max="250">
                                </div>
                                <script>
                                    var slider = document.getElementById("BPMslider");

                                    var output = document.getElementById("BPMvalue");

                                    output.innerHTML = slider.value;

                                    slider.oninput = function () {
                                        output.innerHTML = this.value;
                                    }
                                </script>
                                <div class="row">
                                    <div class="col">
                                        <h2 class="optionsLabel float-left">Key</h2>
                                    </div>
                                    <div class="col col-sm-4">
                                        <select class="custom-select form-control-lg" id="keySelect"></select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <h2 class="optionsLabel float-left">Scale</h2>
                                    </div>
                                    <div class="col col-sm-4">
                                        <select class="custom-select form-control-lg" id="scaleSelect"></select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <h2 class="optionsLabel float-left">First Octave</h2>
                                    </div>
                                    <div class="col col-sm-4">
                                        <select class="custom-select form-control-lg"
                                                id="firstOctaveSelect"></select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <h2 class="optionsLabel float-left">Last Octave</h2>
                                    </div>
                                    <div class="col col-sm-4">
                                        <select class="custom-select form-control-lg"
                                                id="lastOctaveSelect"></select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <h2 class="optionsLabel float-left">Bars</h2>
                                    </div>
                                    <div class="col col-sm-4">
                                        <select class="custom-select form-control-lg" id="barsSelect"></select>
                                    </div>
                                </div><!--
                                <div class="row">
                                    <div class="col col-sm-8">
                                        <h2 class="optionsLabel float-left">Humanize</h2>
                                    </div>
                                    <div class="col-sm-2">
                                        <input class="float-left" type="checkbox" value="" id="humanizeCheck"/>
                                    </div>
                                </div>
-->
                                <div class="row">
                                    <div class="col">
                                        <h2 class="optionsLabel float-left">Load Example</h2>
                                    </div>
                                    <div class="col col-sm-4">
                                        <select class="custom-select form-control-lg" id="exampleSelect"></select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="col text-center">
                        <button id="modalClose" type="button" class="btn btn-primary" data-dismiss="modal">
                            <span class="glyphicon glyphicon-ok"></span>
                        </button>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <!-- Bottom Navbar -->
    <nav class="navbar navbar-expand bg-light fixed-bottom">

        <div class="btn-block" role="toolbar">

            <button id="togglePlayBtn" type="button" class="btn btn-outline-primary float-left"
                    onclick="gui.togglePlaying()">
                <span class="glyphicon glyphicon-play"></span>
            </button>

            <div class="btn-group mr-2" role="group" id="instruments">
                <!--overwritten by javascript-->
            </div>
            <script>
                $(function () {
                    var i = 0;
                    $.each(ssi, function (key, value) {
                        if(key != "percussion" && key != 'drumkit') {
                            $('#instruments').append('<button type="button" id="instrumentBtn' + i + '" class="instrument btn btn-light" >' + key + '</button>');
                            $('#instrumentBtn0').removeClass('btn-light').addClass('btn-primary');
                        }
                        i++;
                    });
                    // Wechsel des Instruments
                    $('.instrument').click(function () {

                        var num = this.id.replace('instrumentBtn', '');
                        var i = 0;

                        $.each(ssi, function (key, value) {
                            if(i == num) {
                                gui.currentInstrument = key;
                                gui.drawSequencerGrid();
                            }
                            i++;
                        });


                        $('.instrument').removeClass('btn-primary btn-warning btn-success').addClass('btn-light');

                        if (gui.currentInstrument == "piano" ) {
                            $('#instrumentBtn0').removeClass('btn-light').addClass('btn-primary'); <!-- blau -->
                        }
                        else if (gui.currentInstrument == "marimba" ) {
                            $('#instrumentBtn2').removeClass('btn-light').addClass('btn-warning'); <!-- gelb -->
                        }
                        else if (gui.currentInstrument == "bass" ) {
                            $('#instrumentBtn4').removeClass('btn-light').addClass('btn-success'); <!-- grün -->
                        }

                        $(this).removeClass('btn-light').addClass('btn-primary');
                    });
                });
            </script>

            <!--TO do-->
            <button type="button" class="btn btn-light"
                    onclick="allParts.clearAllParts(); gui.drawSequencerGrid()">
                <span class="glyphicon glyphicon-trash"></span>
            </button>


            <button id="toolsButton" type="button" class="btn btn-outline-primary float-right" data-toggle="modal"
                    href="#" data-target="#myModal" onclick=""><span class="glyphicon glyphicon-cog"></span>
            </button>

        </div>

    </nav>
</div>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!---->


<!-- Optional JavaScript -->
<?php
require_once "./server_side_instruments.php";
?>
<script>
    /**
     * draws the grids for first time use
     * */
    $(function () {
        gui = new GUI();
        gui.drawSequencerGrid();
        gui.drawDrumkitGrid();
        gui.arrayBuilder(14, 16);
    });

    /**
     * ServerSidedInstruments = ssi
     * the js variable "ssi" will be filled with the sounds which are placed on the server
     * they will be usable in js from now on
     * */
    var ssi = <?php echo get_server_side_instruments(); ?>;

</script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script src="js/Tone.js"></script>
<script src="js/instrument.js"></script>
<script src="js/scales.js"></script>
<script src="js/settings.js"></script>
<script src="js/sequencer.js"></script>
<script src="js/GUI.js"></script>
<script src="js/modal.js"></script>
</body>

</html>
